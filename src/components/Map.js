import React, {useContext, useMemo, useState} from "react";
import {filterGeojson,} from '../utils/utils';
import 'mapbox-gl/dist/mapbox-gl.css';
import MapGL, {
    AttributionControl,
    FullscreenControl,
    GeolocateControl,
    LanguageControl,
    Layer,
    NavigationControl,
    Popup,
    ScaleControl,
    Source
} from '@urbica/react-map-gl';
import {ControllerContext} from '../context/ControllerState';
import {MapContext} from '../context/MapState';
import {colors} from '../constants/Constants';


export default function Map() {
    const {priceRange, buildingTypes, parkingExist} = useContext(ControllerContext);
    const {propertiesData, language,theme} = useContext(MapContext);
    const [popupValue, setPopupValue] = useState({})

    const [viewport, setViewport] = useState({
        latitude: 47.38364821666721,
        longitude: 8.491784286499023,
        zoom: 12
    });

    const clickPopup = (e) => {
        const {geometry, properties} = e;
        setPopupValue({geometry, properties})
    }

    const memoizedFilteredList = useMemo(
        () => filterGeojson(propertiesData, {priceRange, buildingTypes, parkingExist}),
        [propertiesData, priceRange, buildingTypes, parkingExist]
    );

    return (
        <>
            <MapGL
                style={{width: '100%', height: '100vh'}}
                mapStyle={theme}
                accessToken={process.env.REACT_APP_MAPBOX_TOKEN}
                latitude={viewport.latitude}
                longitude={viewport.longitude}
                zoom={viewport.zoom}
                onViewportChange={setViewport}
                attributionControl={false}
            >
                <Source id='points' type='geojson' data={memoizedFilteredList}/>
                {/* I have use a layer instead of just injecting the data so we can manipulate / visualize /style for
                    like symbol, raster, circle, fill-extrusion, heatmap, hillshade */}
                <Layer
                    onClick={(e) => clickPopup(...e.features)}
                    id='Price'
                    type='circle'
                    source='points'
                    paint={{
                        'circle-radius': 9,
                        'circle-stroke-width': 1,
                        'circle-stroke-color': colors[8],
                        'circle-opacity': 0.8,
                        'circle-color': [
                            'match',
                            ['get', 'BuildingType'],
                            'Residential',
                            colors[0],
                            'Commercial',
                            colors[2],
                            'Industrial',
                            colors[3],
                            'Offices',
                            colors[1],
                            'Mixed use',
                            colors[4],
                            '#000'

                        ]
                    }}
                />
                {/*map controller components component */}
                <LanguageControl language={language === 'en-us' ? 'en':'de'} defaultLanguage='en'/>
                <NavigationControl showCompass showZoom position='top-right'/>
                <ScaleControl unit='metric' position='bottom-right'/>
                <FullscreenControl position='top-right'/>
                <GeolocateControl position='top-right'/>
                <AttributionControl
                    compact={false}
                    position='bottom-right'
                    customAttribution='<img src="svg6.svg" alt="nomoko logo">
<b>Nomoko.world</b> test app'
                />
                {/*popup component */}
                {popupValue.geometry && (
                    <Popup longitude={popupValue.geometry?.coordinates[0]}
                           latitude={popupValue.geometry?.coordinates[1]}
                           closeButton={true}
                           closeOnClick={false}
                           onClose={() => {
                               setPopupValue({});
                           }}>
                        {`Building Type \n: ${popupValue.properties.BuildingType} \
                          Parking :${popupValue.properties.Parking} \r
                          Price/m^2 :${popupValue.properties['Price/m^2']}`}
                        {}
                    </Popup>
                )
                }
            </MapGL>
        </>
    );
}
