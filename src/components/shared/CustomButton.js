import React from 'react';
import {Button, Popup} from 'semantic-ui-react';

function CustomButton({
                          content,
                          circular,
                          popupContent,
                          handler,
                          name,
                          toggle,
                          active,
                          icon,
                          value,
                          disabled,
                          basic,
                          size,
                          loading,
                          color
                      }) {

    return (
        <Popup
            content={popupContent}
            size={size}
            trigger={
                <Button
                    color={color}
                    circular={circular}
                    content={content}
                    loading={loading}
                    name={name}
                    toggle={toggle}
                    active={active}
                    size={size}
                    onClick={handler}
                    basic={basic}
                    disabled={disabled}
                    icon={icon}
                />
            }
        />
    );
}

export default CustomButton;
