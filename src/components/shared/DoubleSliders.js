import React, {useState} from "react";
import {Slider} from "react-semantic-ui-range";
import Label from 'semantic-ui-react/dist/commonjs/elements/Label';
import PropTypes from 'prop-types';
import CustomLabel from './CustomLabel';

export const DoubleSliders = ({start, step, dispatch, currency}: Props) => {
    const [minMax, setMinMax] = useState(start)
    return (
        <>
            {minMax[0] && (
                <Slider
                    multiple
                    value={start}
                    color="red"
                    settings={{
                        start: start,
                        min: minMax[0],
                        max: minMax[1],
                        step: step,
                        onChange: value => {
                            dispatch(value);
                        }
                    }}
                />

            )}
            <div className="mt2 flex flex-row justify-between">
                <Label color="red">{currency} {start[0]}</Label>
                <div className="mt2">
                    <CustomLabel
                        value={'Price Range'}
                        position={'top center'}
                        content={
                            'Cheapest to most expensive'
                        }
                    />
                </div>
                <Label color="red">{currency} {start[1]}</Label>
            </div>

        </>
    );
};

DoubleSliders.propTypes = {
    step: PropTypes.number,
    start: PropTypes.array,
    dispatch: PropTypes.func,
    currency: PropTypes.string
};
