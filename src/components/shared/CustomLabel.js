import React from 'react';
import PropTypes from 'prop-types';
import {Label, Popup} from 'semantic-ui-react';


function CustomLabel({content, value, position}) {
    return (
        <Popup content={content}
               position={position}
               trigger={<Label size="tiny">{value}</Label>}/>
    );
}

export default CustomLabel;

CustomLabel.propTypes = {
    content: PropTypes.string,
    value: PropTypes.string,
    position: PropTypes.string
}
