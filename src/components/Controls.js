import React, {useContext, useState} from 'react'
import {Divider, Dropdown, Header, Radio, Segment} from 'semantic-ui-react'
import {DoubleSliders} from './shared/DoubleSliders';
import {ControllerContext} from '../context/ControllerState';
import CustomLabel from './shared/CustomLabel';
import CustomButton from './shared/CustomButton';
import {MapContext} from '../context/MapState';
import {translate} from '../i18n/translate';
import {currency, languageOptions, themeOptions} from '../constants/Constants';
import "../stylesheet/Control.scss";

function Controls() {
    const {changePriceRange, priceRange, buildingTypes, togglebuildingType, toggleParking, parkingExist, allParking, resetOriginalState} = useContext(ControllerContext);
    const {changeLanguage, changeMapTheme} = useContext(MapContext);

    return (
        <>
            <Segment id="controlSegment">
                <div>
                    <div className="flex flex-row justify-between">
                        <Dropdown
                            button
                            className='icon'
                            floating
                            labeled
                            icon='world'
                            options={languageOptions}
                            search
                            text='Select Language'
                            onChange={changeLanguage}
                        />
                        <Dropdown
                            button
                            className='icon'
                            floating
                            labeled
                            icon='world'
                            options={themeOptions}
                            search
                            text='Select Map Theme'
                            onChange={changeMapTheme}
                        />
                    </div>
                    <span>
                        <Divider/>
              <Header as="h4">{translate("app_title")}</Header>
<p> > Data-driven styling</p>
<p> > Price range/slider with auto-calculated min and max </p>
<p> > Toggle to switch for properties with parking or not
or both (with and without parking)</p>
<p> > All unique properties type dynamically
 filtered and user can activate and deactivate, if you try to add a new property type
automatically will be there as a new button</p>
<p> > Reset option to reset all filters</p>
<p> > Language EN/DE and map toggle Light/Dark </p>
            </span>
                </div>
                <Header as="h5">{translate("price_range_slider")}</Header>
                <div className="flex flex-row">
                    <div className="w-100">
                        {priceRange[0] &&
                        <DoubleSliders
                            start={priceRange}
                            min={priceRange[0]}
                            max={priceRange[1]}
                            currency={currency}
                            step={300}
                            dispatch={changePriceRange}
                        />
                        }
                    </div>
                </div>
                <Divider/>
                <div className="flex flex-row justify-between">
                    <div className="w-30">
                        <Segment>
                            <div className="flex flex-row justify-around">
                                <Radio onClick={allParking} checked={parkingExist === null ? true : false}/>
                                <i className="small icons">
                                    <i aria-hidden="true" className="red dont big icon"/>
                                    <i aria-hidden="true" className="black car icon"/></i>
                                <i className="small icons">
                                    <i aria-hidden="true" className="notch big  icon">
                                    </i>
                                    <i aria-hidden="true" className="plus icon"/></i>
                                <i className="small icons">
                                    <i aria-hidden="true" className="circle notch big loading icon">
                                    </i>
                                    <i aria-hidden="true" className="car icon"/></i>
                            </div>
                        </Segment>
                    </div>
                    <div className="w-10">
                        <CustomLabel
                            value={'Parking information'}
                            position={'top center'}
                            content={
                                "Left side with AND without parking and right side with OR without car'."
                            }
                        />
                    </div>
                    <div className="w-40">
                        <Segment>
                            <div className="flex flex-row justify-around">
                                <Radio onChange={toggleParking} toggle/>
                                <i className="small icons">
                                    <i aria-hidden="true" className="red dont big icon"/>
                                    <i aria-hidden="true" className="black car icon"/></i>
                                <i className="small icons">
                                    <i aria-hidden="true" className=" notch big  icon">
                                    </i>
                                    <i aria-hidden="true" className="arrows alternate horizontal icon"/></i>
                                <i className="small icons">
                                    <i aria-hidden="true" className="circle notch big loading icon">
                                    </i>
                                    <i aria-hidden="true" className="car icon"/></i>
                            </div>
                        </Segment>
                    </div>
                </div>
                <Divider/>
                <div>
                    {Object.keys(buildingTypes).map((key, index) => {
                        let content = buildingTypes[key].type
                        return (
                            <div key={index} className="mt1 dib">
                                <CustomButton
                                    icon={buildingTypes[key].disabled ? 'plus': 'delete'}
                                    popupContent={'Enable/disable this property Type'}
                                    content={content}
                                    name={buildingTypes[key].type}
                                    handler={() => {
                                        togglebuildingType(buildingTypes[key])
                                    }}
                                    color={buildingTypes[key].color}
                                    loading={false
                                    }
                                    size="tiny"
                                />
                            </div>
                        )
                    })}
                    <Divider/>
                    <div className="mt2">
                        <CustomButton
                            icon={'remove'}
                            size={'tiny'}
                            content={'Reset all enabled filters'}
                            popupContent={'Reset all filters to initial state'}
                            handler={resetOriginalState}
                            disabled={false}
                        />
                    </div>
                </div>
            </Segment>
        </>
    );
}

export default Controls;
