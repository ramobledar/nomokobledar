import React from 'react';
import Map from './components/Map';
import Controls from './components/Controls';
import {ControllerProvider} from './context/ControllerState';
import {MapProvider} from './context/MapState';

function App() {
    return (
        <div>
            <MapProvider>
                <ControllerProvider>
                    <Map/>
                    <Controls/>
                </ControllerProvider>
            </MapProvider>
        </div>
    );
}

export default App;
