import axios from "axios";
import * as CSVFile from '../data/properties_f.csv';
import {csvToJson, jsonToGeoJSON} from '../utils/utils';
import BaseClient from './BaseClient';

const axiosInstance = axios.create({
  baseURL: "./data/",
});

const apiClient = new BaseClient(axiosInstance);

export async function fetchProperties() {
  try {
      // In real world app fetching directly from fakeApi would be the case
      // const response = await apiClient.get(axiosInstance.baseURL);

      // for our testing purposes we just use a csv static file
      const csvPropertiesData = await fetch(CSVFile).then((response) => response.text())
      const  jsonPropertiesData  = csvToJson(csvPropertiesData)
      // json is more than enough for our purpose but Im transforming it to
      // geojson so we can extend with more geospation features in  future and make yse of libraries like turf
      return  jsonToGeoJSON(jsonPropertiesData);
  } catch (error) {
    return { error };
  }
}
