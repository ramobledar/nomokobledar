import {colors} from '../constants/Constants';

const formatCoordinates = (point) => {
    return point.substring(
        point.lastIndexOf('(') + 1,
        point.lastIndexOf(')'),
    ).split(' ');
};

const mapProperties = (properties) =>{
    return properties.map(pro => {
        return {
            ...pro,
            Parking: Boolean(pro.Parking),
            'Price/m^2': parseFloat(pro['Price/m^2']),
            Coordinates: formatCoordinates(pro.Coordinates)
        }
    });
}

export const csvToJson = (csvText) => {
    let lines = [];
    const linesArray = csvText.split('\n');
    linesArray.forEach((e) => {
        const row = e.replace(/[\s]+[,]+|[,]+[\s]+/g, ',').trim();
        lines.push(row);
    });
    const result = [];
    const headers = lines[0].split(";");
    for (let i = 1; i < lines.length; i++) {
        const obj = {};
        const currentline = lines[i].split(";");
        for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }
        result.push(obj);
    }
    return mapProperties(result)
}


export const jsonToGeoJSON = (json) => {
    return {
        "type": "FeatureCollection",
        "features": json.map(feature => {
            return {
                "type": "Feature",
                properties: feature,
                "geometry": {
                    "type": "Point",
                    "coordinates": [feature.Coordinates[1], feature.Coordinates[0]]
                }
            }
        })
    }
}

export const pickAColor = (index) => {
    return colors.find((color, i) => index === i)
}

export const findMinMax = (arr, property) => {
    let min = arr[0][property], max = arr[0][property];
    for (let i = 1, len = arr.length; i < len; i++) {
        let v = arr[i][property];
        min = (v < min) ? v : min;
        max = (v > max) ? v : max;
    }
    return [min, max];
}


export const filterGeojson = (allProperties, params) => {
    const deactivatedBuildingTypes = params.buildingTypes.reduce((acc, x) => (!x.disabled ? [...acc, x.type] : acc), []);
    if (allProperties.features) {
        return {
            "type": "FeatureCollection",
            "features": allProperties.features.filter(function (el) {
                return el.properties['Price/m^2'] >= params.priceRange[0]
                    && el.properties['Price/m^2'] <= params.priceRange[1]
                    && (typeof params.parkingExist == 'boolean' ? el.properties['Parking'] === params.parkingExist : true)
                    && deactivatedBuildingTypes.includes(el.properties.BuildingType)
            })
        }
    }
}


export const uniqueBuildingTypes = (allBuildingTypes) => {
    return [...new Set(allBuildingTypes.map(item => item.BuildingType))].filter(Boolean)
}

