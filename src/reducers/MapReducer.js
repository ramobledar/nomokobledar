export default (state, action) => {
    switch (action.type) {
        case "ADD_PROPERTIES":
            return {
                ...state,
                propertiesData: action.payload, ...state.propertiesData,
            };
        case "SET_MAP_THEME":
            return {
                ...state,
                theme:action.payload , ...state.theme
            };
        case "SET_LANGUAGE":
            return {
                ...state,
                language:action.payload , ...state.language
            };
        default:
            return state;
    }
};
