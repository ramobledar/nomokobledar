export default (state, action) => {
    switch (action.type) {
        case "ADD_BUILDING_TYPES":
            return {
                ...state,
                buildingTypes: action.payload, ...state.buildingTypes,
            };
        case "DISABLE_BUILDING_TYPES":
            return {
                ...state,
                buildingTypes:state.buildingTypes.map(obj =>
                    obj.type === action.payload.type ? { ...obj, disabled: !action.payload.disabled } : obj
                )
            };
        case "CHANGE_PRICE_RANGE":
            return {
                ...state,
                priceRange:action.payload , ...state.priceRange
            };
        case "TOGGLE_PARKING":
            return {
                ...state,
                parkingExist:action.payload , ...state.parkingExist
            };
        case "CHANGE_LANGUAGE":
            return {
                ...state,
                language:action.payload , ...state.language
            };
        default:
            return state;
    }
};
