import React, {createContext, useEffect, useReducer, useState} from "react";
import {fetchProperties} from '../fakeApi/ApiCalls';
import TranslationProvider from '../i18n/Provider';
import MapReducer from '../reducers/MapReducer';
import {LOCALES} from '../constants/Constants';

const mapInitialState = {
    propertiesData: [],
    language: LOCALES.ENGLISH,
    theme: 'mapbox://styles/mapbox/light-v9'
};

export const MapContext = createContext();
export const MapProvider = ({children}) => {


    const [mapState, dispatch] = useReducer(MapReducer, mapInitialState);

    useEffect(() => {
        fetchProperties().then((propertiesData) => {
            dispatch({
                type: "ADD_PROPERTIES",
                payload: propertiesData
            });
        })
    }, []);

    const changeLanguage = (event, data) => {
        dispatch({
            type: "SET_LANGUAGE",
            payload: data.value
        });
    };

    const changeMapTheme = (event, data) => {
        dispatch({
            type: "SET_MAP_THEME",
            payload: data.value
        });
    };

    return (
        <MapContext.Provider
            value={{changeLanguage,changeMapTheme, ...mapState}}>
            <TranslationProvider locale={mapState.language}>
                {children}
            </TranslationProvider>
        </MapContext.Provider>
    );
};
