import React, {createContext, useContext, useEffect, useReducer, useState} from "react";
import ControllerReducer from "../reducers/ControllerReducer";
import {findMinMax, pickAColor, uniqueBuildingTypes} from '../utils/utils';
import {MapContext} from './MapState';

const controllerInitialState = {
    priceRange: [],
    parkingExist: null,
    buildingTypes: []
};

export const ControllerContext = createContext(controllerInitialState);
export const ControllerProvider = ({children}) => {

    const [controllerState, dispatch] = useReducer(ControllerReducer, controllerInitialState);
    const [originalState, setOriginalState] = useState();
    const {propertiesData} = useContext(MapContext);


    useEffect(() => {
        if (propertiesData.features) {
            const properties = propertiesData.features.map(properties => properties.properties)
            buildingTypes(properties)
            changePriceRange(findMinMax(properties, 'Price/m^2'))
            const copyOfOriginalState = {
                priceRange: findMinMax(properties, 'Price/m^2'),
                buildingTypes: properties,
                parkingExist: null
            }
            setOriginalState(copyOfOriginalState)
        }
    }, [propertiesData]);


    // Actions
    function changePriceRange(propery) {
        dispatch({
            type: "CHANGE_PRICE_RANGE",
            payload: propery
        });
    }

    function resetOriginalState() {
        buildingTypes(originalState.buildingTypes)
        changePriceRange(originalState.priceRange)
        toggleParking(originalState.parkingExist)
    }

    function buildingTypes(json) {
        dispatch({
            type: "ADD_BUILDING_TYPES",
            payload: [...uniqueBuildingTypes(json).map((type, i) => ({type, color: pickAColor(i), disabled: false}))],
        });
    }

    function togglebuildingType(buildingType) {
        dispatch({
            type: "DISABLE_BUILDING_TYPES",
            payload: buildingType,
        });
    }

    function toggleParking(resetValue?) {
        dispatch({
            type: "TOGGLE_PARKING",
            payload: resetValue === null ? resetValue : controllerState.parkingExist === null ? true : !controllerState.parkingExist,
        });
    }

    function allParking() {
        dispatch({
            type: "TOGGLE_PARKING",
            payload: null,
        });
    }

    return (
        <ControllerContext.Provider
            value={{
                priceRange: controllerState.priceRange,
                parkingExist: controllerState.parkingExist,
                buildingTypes: controllerState.buildingTypes,
                language:controllerState.language,
                togglebuildingType,
                changePriceRange,
                toggleParking,
                allParking,
                resetOriginalState,
            }}
        >
            {children}
        </ControllerContext.Provider>
    );
};
