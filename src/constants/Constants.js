export const LOCALES = {
  ENGLISH: "en-us",
  GERMAN: "de-de",
};

export const languageOptions = [
  {key: "English", text: "English", value: LOCALES.ENGLISH},
  {key: "German", text: "German", value: LOCALES.GERMAN}
];

export const themeOptions = [
  {key: "light", text: "Light Theme", value: 'mapbox://styles/mapbox/light-v9'},
  {key: "dark", text: "Dark Theme", value: 'mapbox://styles/mapbox/dark-v9'}
];


export const colors = ["red", "orange", "yellow", "olive", "green", "teal", "black", "pink",'white'];

export const currency = 'CHF'
