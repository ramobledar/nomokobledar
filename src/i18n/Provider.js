import React, {Fragment} from "react";
import { IntlProvider } from "react-intl";
import messages from "./languages/index";
import {LOCALES} from '../constants/Constants';

const Provider = ({ children, locale = LOCALES.ENGLISH }) => (
  <IntlProvider
    textComponent={Fragment}
    locale={locale}
    messages={messages[locale]}
  >
    {children}
  </IntlProvider>
);

export default Provider;
