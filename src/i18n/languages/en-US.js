import {LOCALES} from '../../constants/Constants';

export default {
  [LOCALES.ENGLISH]: {
    app_title: "Nomoko test application",
    price_range_slider: "Price range slider from min to max"
  },
};
