import {LOCALES} from '../../constants/Constants';

export default {
  [LOCALES.GERMAN]: {
    app_title: "Nomoko Testanwendung",
    price_range_slider: "Preisspanne Schieberegler von min bis max"
  },
};
